from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from account_app.models import NewUser, Doc
from django.http import HttpResponse
import django.utils.timezone as timezone
from datetime import timedelta


def index(request):
    user = request.user
    if user.is_authenticated:
        # Logged In
        # home
        context = dict()
        context['user'] = user
        context['tillDocNum'] = 0
        if user.is_manager:
            # manager

            n = Doc.objects.filter(till_time__range=(timezone.now(), timezone.now() + timedelta(days=7)),
                                      active=True, shenhe1=True, shenhe2=True).count()
            context['tillDocNum'] = n
            return render(request, 'manager.html', context)
        if user.is_staff:
            # staff

            return render(request, 'leader.html', context)

        return render(request, 'worker.html', context)
    else:
        # Not Logged In
        return redirect('/login')


def login_view(request):
    context = dict()
    if request.user.is_authenticated:
        # User Logged In Should Log out First
        return redirect('/')
    if request.method == 'GET':
        return render(request, 'login.html')
    else:
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(email=email, password=password)
        if user:
            # Success!
            login(request, user)
            return redirect('/')
        else:
            # Failed
            context['error'] = "Please confirm your E-mail address and password. " \
                               "Maybe there is something wrong with them. " \
                               "Then try again. "
            return render(request, 'error.html', context)


def logout_view(request):
    logout(request)
    return redirect('/login')


def register(request):
    context = dict()
    if request.user.is_authenticated:
        # User Logged In Should Log out First
        return redirect('/')

    if request.method == 'GET':
        return render(request, 'register.html')

    email = request.POST['email']
    password = request.POST['password']
    password2 = request.POST['password2']

    # Check if Passwords entered are the same Here.
    if password != password2:
        context['error'] = "Please enter your password and make sure the what you entered both times are the same. "
        return render(request, 'error.html', context)

    NewUser.object.create_user(email, password)
    return render(request, 'regSucceed.html')


def table_contract(request):
    user = request.user
    if user.is_authenticated:
        # Logged In
        # home
        context = dict()
        if user.is_manager:
            # manager
            context['me'] = 'manager'
            contracts = Doc.objects.filter(active=True, type='contract', shenhe1=True, shenhe2=False)
            contracts_p = Doc.objects.filter(active=True, type='contract', shenhe1=True, shenhe2=True)
            context['contracts'] = contracts
            context['contracts_p'] = contracts_p
        elif user.is_staff:
            # leader
            context['me'] = 'leader'
            contracts = Doc.objects.filter(active=True, type='contract', shenhe1=False)
            contracts_p = Doc.objects.filter(type='contract', shenhe1=True)
            context['contracts'] = contracts
            context['contracts_p'] = contracts_p
        return render(request, 'tableContract.html', context)
    else:
        # Not Logged In
        return redirect('/login')


def table_contract_t(request):
    user = request.user
    if user.is_authenticated:
        # Logged In
        # home
        context = dict()
        if user.is_manager:
            # manager
            context['me'] = 'manager'
            contracts_t = Doc.objects.filter(active=True, type='contract_t', shenhe1=True, shenhe2=False)
            contracts_t_p = Doc.objects.filter(active=True, type='contract_t', shenhe1=True, shenhe2=True)
            context['contracts_t'] = contracts_t
            context['contracts_t_p'] = contracts_t_p
        elif user.is_staff:
            # leader
            context['me'] = 'leader'
            contracts_t = Doc.objects.filter(active=True, type='contract_t', shenhe1=False)
            contracts_t_p = Doc.objects.filter(type='contract_t', shenhe1=True)
            context['contracts_t'] = contracts_t
            context['contracts_t_p'] = contracts_t_p
        return render(request, 'tableContractTemplate.html', context)
    else:
        # Not Logged In
        return redirect('/login')


def table_letter(request):
    user = request.user
    if user.is_authenticated:
        # Logged In
        # home
        context = dict()
        if user.is_manager:
            # manager
            context['me'] = 'manager'
            letters = Doc.objects.filter(active=True, type='letter', shenhe1=True, shenhe2=False)
            letters_p = Doc.objects.filter(active=True, type='letter', shenhe1=True, shenhe2=True)
            context['letters'] = letters
            context['letters_p'] = letters_p
        elif user.is_staff:
            # leader
            context['me'] = 'leader'
            letters = Doc.objects.filter(active=True, type='letter', shenhe1=False)
            letters_p = Doc.objects.filter(type='letter', shenhe1=True)
            context['contracts'] = letters
            context['contracts_p'] = letters_p
        return render(request, 'tableLetter.html', context)
    else:
        # Not Logged In
        return redirect('/login')


def table_trans(request):
    user = request.user
    if user.is_authenticated:
        # Logged In
        # home
        context = dict()
        if user.is_manager:
            # manager
            context['me'] = 'manager'
            transes = Doc.objects.filter(active=True, type='trans', shenhe1=True, shenhe2=False)
            transes_p = Doc.objects.filter(active=True, type='trans', shenhe1=True, shenhe2=True)
            context['transes'] = transes
            context['transes_p'] = transes_p
        elif user.is_staff:
            # leader
            context['me'] = 'leader'
            transes = Doc.objects.filter(active=True, type='trans', shenhe1=False)
            transes_p = Doc.objects.filter(type='trans', shenhe1=True)
            context['transes'] = transes
            context['transes_p'] = transes_p
        return render(request, 'tableTrans.html', context)
    else:
        # Not Logged In
        return redirect('/login')


def table_power(request):
    user = request.user
    if user.is_authenticated:
        # Logged In
        # home
        context = dict()
        if user.is_manager:
            # manager
            context['me'] = 'manager'
            powers = Doc.objects.filter(active=True, type='power', shenhe1=True, shenhe2=False)
            powers_p = Doc.objects.filter(active=True, type='power', shenhe1=True, shenhe2=True)
            context['powers'] = powers
            context['powers_p'] = powers_p
        elif user.is_staff:
            # leader
            context['me'] = 'leader'
            powers = Doc.objects.filter(active=True, type='power', shenhe1=False)
            powers_p = Doc.objects.filter(type='power', shenhe1=True)
            context['powers'] = powers
            context['poweres_p'] = powers_p
            context['me'] = 'leader'
        return render(request, 'tablePower.html', context)
    else:
        # Not Logged In
        return redirect('/login')


def doc_detail(request, id):
    detail = Doc.objects.get(id=id)

    context = dict()
    context['detail'] = detail
    return render(request, 'Detail.html', context)


def doc_submit_pass(request, id):
    user = request.user
    if user.is_authenticated:
        # Logged In
        # home
        context = dict()
        if user.is_manager:
            # manager
            detail = Doc.objects.get(id=id)
            detail.shenhe2 = True
            detail.save()

        elif user.is_staff:
            # leader
            detail = Doc.objects.get(id=id)
            detail.shenhe1 = True
            detail.save()
        return HttpResponse("<script>parent.location.reload();</script>")
    else:
        # Not Logged In
        return redirect('/login')


def doc_submit_notpass(request, id):
    user = request.user
    if user.is_authenticated:
        # Logged In
        # home
        context = dict()
        if user.is_manager:
            # manager
            detail = Doc.objects.get(id=id)
            detail.shenhe2 = True
            detail.active = False
            detail.save()

        elif user.is_staff:
            # leader
            detail = Doc.objects.get(id=id)
            detail.shenhe1 = True
            detail.active = False
            detail.save()
        return HttpResponse("<script>parent.location.reload();</script>")
    else:
        # Not Logged In
        return redirect('/login')


def doc_list(request):
    user = request.user
    if user.is_authenticated:
        # Logged In
        # home
        context = dict()

        context['me'] = 'worker'
        docs = Doc.objects.filter(creator=user)
        context['docs'] = docs
        return render(request, 'tableDoc.html', context)

    else:
        # Not Logged In
        return redirect('/login')


def doc_create(request, type):
    detail = dict()
    if request.method == 'GET':
        if type == 1:
            detail['type'] = "contract"
            detail['templates'] = Doc.objects.filter(type='contract_t', shenhe1=True, shenhe2=True, active=True)
        elif type == 2:
            detail['type'] = "contract_t"
        elif type == 3:
            detail['type'] = "letter"
        elif type == 4:
            detail['type'] = "trans"
        else:
            detail['type'] = "power"
        context = dict()
        context['detail'] = detail
        return render(request, 'Create.html', context)
    else:
        doc_type = request.POST['type']
        doc_name = request.POST['name']
        doc_detail = request.POST['detail']
        doc_till_time = request.POST['tillTime']
        doc_creator = request.user
        new_doc = Doc(type=doc_type, name=doc_name, detail=doc_detail, till_time=doc_till_time, creator=doc_creator)
        new_doc.save()
        return redirect('/')


def doc_create_tem(request, type, template):

    if request.method == 'GET':
        detail = dict()
        tem = Doc.objects.get(id=template)
        detail['template_now'] = tem
        detail['templates'] = Doc.objects.filter(type='contract_t')
        detail['type'] = "contract"
        detail['title'] = tem.name
        detail['content'] = tem.detail
        detail['tillTime'] = tem.till_time

        context = dict()
        context['detail'] = detail

        return render(request, 'Create.html', context)
    else:
        return doc_create(request, type)


def table_leader(request):
    context = dict()
    leaders = NewUser.object.filter(is_staff=True, is_admin=False)
    context['leaders'] = leaders
    return render(request, 'tableLeader.html', context)


def table_worker(request):
    context = dict()
    workers = NewUser.object.filter(is_staff=False, is_manager=False)
    context['workers'] = workers
    return render(request, 'tableWorker.html', context)


def table_till(request):
    user = request.user
    if user.is_authenticated:
        # Logged In
        # home
        context = dict()

        context['me'] = 'worker'
        docs = Doc.objects.filter(till_time__range=(timezone.now(), timezone.now() + timedelta(days=7)), active=True, shenhe1=True, shenhe2=True)

        context['docs'] = docs
        return render(request, 'tableDoc.html', context)

    else:
        # Not Logged In
        return redirect('/login')
