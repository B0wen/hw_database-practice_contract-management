"""my_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from my_app import views

urlpatterns = [
    path('admin', admin.site.urls),
    path('', views.index),
    path('me', views.index),
    path('home', views.index),
    path('login', views.login_view),
    path('logout', views.logout_view),
    path('register', views.register),
    path('tableContract', views.table_contract),
    path('tableContractTemplate', views.table_contract_t),
    path('tableLetter', views.table_letter),
    path('tableTrans', views.table_trans),
    path('tablePower', views.table_power),
    path('Detail/<int:id>', views.doc_detail),
    path('Detail/<int:id>/pass', views.doc_submit_pass),
    path('Detail/<int:id>/notpass', views.doc_submit_notpass),
    path('list', views.doc_list),
    path('create', views.doc_create),
    path('create/<int:type>', views.doc_create),
    path('create/<int:type>/<int:template>', views.doc_create_tem),
    path('tableLeader', views.table_leader),
    path('tableWorker', views.table_worker),
    path('tableTill', views.table_till),

]
