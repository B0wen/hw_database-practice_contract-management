from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


# Create your models here.
class NewUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        # Check Length of Password Here
        if len(password) < 6:
            raise ValueError('Password too short (6 chars. min)')

        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password=password)

        user.is_staff = True
        user.save(using=self._db)
        return user

    def is_staff(self):
        return self.staff()

    def is_manager(self):
        return self.manager()

    def is_admin(self):
        return self.admin()


class NewUser(AbstractBaseUser):
    email = models.EmailField(max_length=100, unique=True)
    password = models.CharField(max_length=128)
    created_at = models.DateTimeField(auto_now_add=True)

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_manager = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    object = NewUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.email

    def get_name(self):
        return self.email

    def get_create_time(self):
        return self.created_at

    def has_perm(self, perm, obj=None):
        return self.is_active and self.is_staff

    def has_module_perms(self, app_label):
        return self.is_staff

    def staff(self):
        return self.is_staff

    def manager(self):
        return self.is_manager

    def admin(self):
        return self.is_admin


class Doc(models.Model):
    type = models.CharField(max_length=30)
    name = models.CharField(max_length=30)
    detail = models.CharField(max_length=100000)
    create_time = models.DateField(auto_now_add=True)
    till_time = models.DateField()
    creator = models.CharField(max_length=30)
    shenhe1 = models.BooleanField(default=False)
    shenhe2 = models.BooleanField(default=False)

    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

