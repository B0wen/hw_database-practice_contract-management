# admin.py
from django.contrib import admin
from account_app.models import NewUser, Doc

# Register your models here.
admin.site.register(NewUser)
admin.site.register(Doc)
